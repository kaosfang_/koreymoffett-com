# README


This is a work in progress but is the source code for www.koreymoffett.com

Technologies used:
- HTML
- CSS & framework: Bulma

This is my personal website:
- Resume
- Blog
- and anything else I feel should be there


There are some unused files, none of the .php files are being used as github pages only supports static web pages.
<br>
Please disregard those files :)
