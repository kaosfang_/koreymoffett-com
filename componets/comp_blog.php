<?php
echo '
    <!-- Blog -->
    <section class="section">
        <div class="container">
            <hr>
            <a href="/blog">
                <h1 class="title">Blog</h1>
            </a><br>
            <div class="notification">
                <a href="/blog/switching-to-emacs">
                    <h1 class="subtitle">Switching to Doom-Emacs - 12/24/20</h1>
                </a>
                <br>
                <a href="/blog/happy-holidays">
                    <h1 class="subtitle">Happy Holidays - 12/25/20</h1>
                </a>
                <br>
                <a href="/blog/ill-never-buy-mac">
                    <h1 class="subtitle">I will never buy a Mac - 12/26/20</h1>
                </a><br>
                <a href="/blog/foss">
                    <h1 class="subtitle">Why use FOSS? - 12/29/20</h1>
                <a>
            </div>
        </div>
    </section>
 ';
    ?>
