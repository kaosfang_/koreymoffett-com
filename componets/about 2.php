
<?php
echo '

 <!-- About section -->

    <section class="section">
        <div class="container">
            <h1 class="title">About</h1><br>
            <div class="notification">
                <h1 class="subtitle">My name is Korey, I enjoy coding and tinkering with technology.<br><br>
                    <h1 class="subtitle">If you would like to contact me <a href="mailto: korey.moffett87@gmail.com">Email</a>, <a href="https://fosstodon.org/@koreymoffett">Mastodon</a> or <a href="https://twitter.com/korey_moffett">Twitter</a> are the best ways to do so.</h1>
                </h1>
            </div>
        </div>
    </section>
'
?>
