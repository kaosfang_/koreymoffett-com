<?php
echo '
    <!-- Support section -->

    <section class="section">
        <div class="container">
            <hr>
            <h1 class="title">Support</h1><br>
            <div class="notification mb-6">
                <h1 class="subtitle">Any and all support is tremendously appreciated, Thank you! <br><br></h1>

<script src="https://liberapay.com/KaosFang/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/KaosFang/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>
<br>
<br>

                <a href="https://www.buymeacoffee.com/korey"><img src="https://img.buymeacoffee.com/button-api/?text=Buy me a coffee&emoji=&slug=korey&button_colour=FF5F5F&font_colour=ffffff&font_family=Poppins&outline_colour=000000&coffee_colour=FFDD00"></a>

            </div>
        </div>
    </section>
';
    ?>
