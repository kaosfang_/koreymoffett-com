<?php
echo '
<!-- Projects section -->
    </section>
    <section class="section">
        <div class="container">
            <hr>
            <h1 class="title">Projects</h1>
            <h1 class="subtitle">This section will updated as I make more projects</h1>
            <div class="notification">
                <img src="images/gimp.png">
                <p class="subtitle">Here is the source code for this site.</p>
                <a class="has-text-black" href="https://github.com/KoreyMoffett/myWeb" target="_Blank"><i class="fa fa-code fa-lg"></i></a>
                <a class="has-text-black" href="https://koreymoffett.com/" target="_Blank"><i class="fa fa-window-maximize fa-lg"></i></a>
            </div>
        </div>

    </section>
    ';

    // PROJECT FRAME
    /*
    <section class="section">
        <div class="container">

            <div class="notification">
                <img src="images/pic1.png">
                <p class="subtitle">This is a little project i have been working on about diffrent terminal shells.
                    <br> This site is still under development.
                </p>
                <a class="has-text-black" href="https://github.com/KoreyMoffett/TerminalNerd" target="_Blank"><i class="fa fa-code fa-lg"></i></a>
                <a class="has-text-black" href="https://koreymoffett.github.io/TerminalNerd/" target="_Blank"><i class="fa fa-window-maximize fa-lg"></i></a>
            </div>
        </div>
    </section>
*/
?>
