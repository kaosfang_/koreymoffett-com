<?php

echo '

    <!-- Skills section -->

    <section class="section">
        <div class="container">
            <hr>
            <h1 class="title">Skills</h1><br>
            <div class="notification">
                <h1 class="subtitle">
                    I have worked with...<br><br>
                    <i class="fab fa-html5"></i>HTML5<br>
                    <i class="fab fa-css3-alt"></i>CSS3<br>
                    <i class="fab fa-js "></i></i>JavaScript<br>
                    <i class="fab fa-python"></i>Python<br>
                    <i class="fab fa-php"></i>PHP<br>
                    <i class="fas fa-gem"></i>Ruby
                </h1>
            </div>
        </div>
    </section>

';



?>
