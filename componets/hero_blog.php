<?php
echo '

    <section class="hero is-black">
        <div class="hero-body">
            <div class="container">
                <a href="/">
                <h1 class="title is-2">
                    Korey Moffett
                </h1>
                <h2 class="subtitle">
                   Blog
                </h2></a>
                <br>
                <nav class="navbar">
                    <span class="icon is-large">
                <a class="has-text-white" href="https://github.com/KoreyMoffett" target="_Blank">
                    <i class="fab fa-github fa-2x"></i>
                </a>


                </span>
                    <span class="icon is-large">
                <a class="has-text-link" href="https://fosstodon.org/@koreymoffett" target="_Blank">
                    <i class="fab fa-mastodon fa-2x"></i>
                </a>
                </span>


                </span>
                    <span class="icon is-large">
                <a class="has-text-danger" href="mailto: korey.moffett87@gmail.com" target="_Blank">
                    <i class="fas fa-envelope-open-text fa-2x"></i>
                </a>
                </span>
                    <span class="icon is-large">
                <a class="has-text-info" href="https://twitter.com/korey_moffett" target="_Blank">
                    <i class="fab fa-twitter fa-2x"></i>
                </a>
                </span>
                </nav>
            </div>
        </section>

';

?>
